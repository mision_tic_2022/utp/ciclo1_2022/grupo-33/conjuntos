
print('------------LISTAS A CONJUNTOS-----------')
lista_numeros_1 = [10,20,30,40,50,60,10,10,10,20,30,40]
lista_numeros_2 = [5,10,15,20,30,40,45,64,70,80]

conjunto_numeros_1 = set(lista_numeros_1)
#print(conjunto_numeros_1)
conjunto_numeros_2 = set(lista_numeros_2)

datos_comun = conjunto_numeros_1.intersection(lista_numeros_2)
print(datos_comun)

lista_datos_comun = list(datos_comun)
print(lista_datos_comun)
#Ordenar de menor a mayor
lista_datos_comun.sort()
print('Menor a mayor:')
print(lista_datos_comun)
lista_datos_comun.sort(reverse=True)
print(lista_datos_comun)

#Unir datos
union = conjunto_numeros_1.union(conjunto_numeros_2)
print('Union-> ', union)

print('------------CADENA DE CARACTERES A CONJUNTOS-----------')
mensaje = 'Hola mundo desde Misión Tic 2022 - UTP'
conjunto = set(mensaje)
print(conjunto)

print('------------TUPLAS A CONJUNTOS-----------')
tupla = ('Juan', 'Pedro', 'María', 'Juliana')
conjunto = set(tupla)
print(conjunto)

print('------------DICCIONARIO A CONJUNTOS-----------')
dict_persona = {
    'nombre': 'María',
    'apellido': 'Ureña',
    'edad': 25,
    'telefono': '123456789'
}
conjunto = set( dict_persona.keys() )
print(conjunto)
conjunto = set(dict_persona.values())
print(conjunto)
print('-----ELIMINAR ELEMENTOS----')
conjunto.remove(25)
print(conjunto)

print('-----AÑADIR ELEMENTOS----')
conjunto = {10,20,30,40}#set()
conjunto.add(10)
conjunto.add(20)
print(conjunto)
conjunto.update([10,90,80,60])

conjunto = set([10,90,80,60])
